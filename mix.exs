defmodule Feedfinder.Mixfile do
  use Mix.Project

  def project do
    [
      app: :feedfinder,
      version: "0.6.2",
      elixir: "~> 1.8",
      start_permanent: Mix.env == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
      {:tesla, "~> 1.3"},
      {:hackney, "~> 1.16"},
      {:feeder_ex, "~> 1.1"},
      {:fuzzyurl, "~> 0.9.0"},
      {:floki, "~> 0.28.0"},
      {:credo, "~> 1.4", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false},
    ]
  end
end
