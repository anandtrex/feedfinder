# Feedfinder

Feedfinder is a library that provides utility functions for finding xml feeds on a website.

## Installation

The package can be installed by adding `feedfinder` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:feedfinder, git: "git@gitlab.com:anandtrex/feedfinder.git", branch: "master"},
  ]
end
```

