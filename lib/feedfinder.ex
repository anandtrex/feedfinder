defmodule HTTPTesla do
  @moduledoc false
  use Tesla

  plug Tesla.Middleware.FollowRedirects, max_redirects: 5
  plug Tesla.Middleware.DecompressResponse
                                           
  adapter Tesla.Adapter.Hackney

  def get_url(url) do
    get(url)
  end
end

defmodule FeedFinder do
  @moduledoc """
  This process does all the work necessary to get the proper xml link when the user subscribes
  to a new feed
  """

  require Logger

  def get_feed_details(feed_url) do
    {body, _url} = get_link_body(feed_url)
    case FeederEx.parse(body) do
      {:ok, feed, _} ->
        %{:title => feed.title, :htmlUrl => feed.link, :url => feed_url}
      {:fatal_error, fatal_error} -> 
        Logger.error "Couldn't parse body by FeederEx. Error #{fatal_error}"
        nil
      error ->
        Logger.error "Unknown error #{inspect(error)} occurred"
        nil
    end
  end

  def get_feed_details(nil), do: nil

  def get_feed_xml_url(new_feed_url) do
    # TODO: Handle parse error
    # TODO: Resolve redirects
    feed_url_f = Fuzzyurl.from_string(new_feed_url)
    Logger.debug "Feed url is (1): #{inspect(feed_url_f)}"

    feed_url_f = if is_nil(feed_url_f.protocol) do
      Logger.debug "Adding protocol"
      %{feed_url_f | protocol: "http"}
    else
      feed_url_f
    end
    Logger.debug "Feed url is (2): #{inspect(feed_url_f)}"
    new_feed_url = Fuzzyurl.to_string(feed_url_f)

    {body, new_feed_url} = get_link_body(new_feed_url)
    feed_xml_url = case body do
        nil -> nil
        _ -> case content_xml?(body) do
          true -> new_feed_url
          false ->
            case find_feed_link_in_html(body) do
              {:ok, feed_link} ->
                Logger.debug "Found feed link"
                # TODO: The page may include a base header. See http://www.rssboard.org/rss-autodiscovery
                feed_link_f = Fuzzyurl.from_string(feed_link)

                protocol = if(is_nil(feed_link_f.protocol), do: feed_url_f.protocol, else: feed_link_f.protocol)
                hostname = if(is_nil(feed_link_f.hostname), do: feed_url_f.hostname, else: feed_link_f.hostname)

                f = Fuzzyurl.new(protocol: protocol, hostname: hostname, path: feed_link_f.path)
                potential_url = Fuzzyurl.to_string(f)
                Logger.debug "Potential feed url is #{potential_url}"
                check_url(potential_url)
              {:error, error_message} -> 
                Logger.error "Feed url for #{new_feed_url} not found. \n #{error_message}"
                # Trying a pattern similar to what https://blog.google.com uses
                rss_appended_feed_url = new_feed_url <> "/rss/"
                feed_appended_feed_url = new_feed_url <> "/feed.xml"
                atom_appended_feed_url = new_feed_url <> "/atom.xml"
                check_url(rss_appended_feed_url) || check_url(feed_appended_feed_url) || check_url(atom_appended_feed_url)
            end
        end
      end

    Logger.debug "Feed link is #{feed_xml_url}"
    feed_xml_url
  end

  defp check_url(potential_url) do
    Logger.debug "Trying for #{potential_url}"
    case content_xml?(elem(get_link_body(potential_url), 0)) do
      true ->
        Logger.debug "Potential feed url #{potential_url} checked out"
        potential_url
      false ->
        Logger.info "Potential feed url #{potential_url} is not a feed xml link"
        nil
    end
  end

  defp get_link_body(link) do
    Logger.debug "Getting link body for #{link}"
    case HTTPTesla.get_url(link) do
      {:ok, resp} -> 
        Logger.debug "Fetched link body successfully"
        {resp.body, resp.url}
      {:error, error_message} ->
        Logger.error "Error: #{inspect(error_message)}"
        {nil, nil}
    end
  end

  defp content_xml?(nil) do
    Logger.debug "nil content is not xml"
    Logger.info "nil body"
    false
  end

  defp content_xml?(body) do
    # Logger.debug "Body is #{body}"
    case FeederEx.parse(body) do
      {:ok, _, _} -> true
      {:fatal_error, fatal_error} -> 
        Logger.error "Couldn't parse body by FeederEx. Error #{fatal_error}"
        false
      error ->
        Logger.error "Unknown error #{inspect(error)} occurred"
        false
    end
  end

  defp find_feed_link_in_html(body) do
    # NOTE: The body may also contain a rel="feed" without content type according to
    # https://blog.whatwg.org/feed-autodiscovery
    # Also in the case of the hAtom format (!!) http://microformats.org/wiki/hatom, the type
    # is text/html !!
    # TODO: Check that the contenttype matches one of atom/rss/html

    Logger.debug "Looking for feed link in html"
    feed_link_1 = body
      |> Floki.parse_document!
      |> Floki.find(~s(link[rel~="alternate"]))
      |> Floki.attribute("href")

    case feed_link_1 do
      [] -> feed_link_2 = body
              |> Floki.parse_document!
              |> Floki.find(~s(link[rel~="feed"]))
              |> Floki.attribute("href")
            if feed_link_2 == [] do
              Logger.debug "Couldn't find feed link"
              {:error, "Not found"}
            else
              Logger.debug "Found feed link #{feed_link_2}"
              {:ok, hd feed_link_2}
            end
      _ -> 
        Logger.debug "Found feed link #{feed_link_1}"
        {:ok, hd feed_link_1}
    end
  end
end
