defmodule FeedfinderTest do
  require Logger
  use ExUnit.Case
  doctest FeedFinder

  test "test nonexistent site" do
    Logger.debug "==========================================================================================="
    assert is_nil(FeedFinder.get_feed_xml_url("blog.gooogle.com"))
    Logger.debug "==========================================================================================="
  end

  test "test rss suffix" do
    Logger.debug "==========================================================================================="
    assert FeedFinder.get_feed_xml_url("blog.google.com") === "https://www.blog.google//rss/"
    Logger.debug "==========================================================================================="
  end

  test "test gzipped site" do
    Logger.debug "==========================================================================================="
    assert FeedFinder.get_feed_xml_url("https://blog.superfeedr.com") === "https://blog.superfeedr.com/atom.xml"
    Logger.debug "==========================================================================================="
  end

  test "test rss site" do
    Logger.debug "==========================================================================================="
    assert FeedFinder.get_feed_xml_url("http://www.rssboard.org") === "http://feeds.rssboard.org/rssboard"
    Logger.debug "==========================================================================================="
  end

  test "test wordpress site" do
    Logger.debug "==========================================================================================="
    assert FeedFinder.get_feed_xml_url("https://printmesomecolor.com") === "https://www.printmesomecolor.com/feed/"
    Logger.debug "==========================================================================================="
  end

  test "test feedburner site" do
    Logger.debug "==========================================================================================="
    assert FeedFinder.get_feed_xml_url("https://feeds.feedburner.com/TheEdublogger") === "https://feeds.feedburner.com/TheEdublogger"
    Logger.debug "==========================================================================================="
  end

end
