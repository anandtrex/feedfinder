
defmodule FeeddetailsTest do
  require Logger
  use ExUnit.Case
  doctest FeedFinder

  test "test rss suffix" do
    Logger.debug "==========================================================================================="
    feed_url = FeedFinder.get_feed_xml_url("blog.google.com")
    feed_details = FeedFinder.get_feed_details(feed_url)
    assert feed_details.htmlUrl === "https://blog.google/"
    assert feed_details.title === "The Keyword"
    Logger.debug "==========================================================================================="
  end

  test "test gzipped site" do
    Logger.debug "==========================================================================================="
    feed_url =  FeedFinder.get_feed_xml_url("https://blog.superfeedr.com")
    feed_details = FeedFinder.get_feed_details(feed_url)
    assert feed_details === %{htmlUrl: "https://superfeedr-blog-feed.herokuapp.com/", title: "Superfeedr Blog"}
    Logger.debug "==========================================================================================="
  end

  test "test rss site" do
    Logger.debug "==========================================================================================="
    feed_url =  FeedFinder.get_feed_xml_url("http://www.rssboard.org")
    feed_details = FeedFinder.get_feed_details(feed_url)
    assert feed_details === %{htmlUrl: "https://www.rssboard.org/", title: "RSS Advisory Board"}
    Logger.debug "==========================================================================================="
  end

  test "test wordpress site" do
    Logger.debug "==========================================================================================="
    feed_url = FeedFinder.get_feed_xml_url("https://printmesomecolor.com")
    feed_details = FeedFinder.get_feed_details(feed_url)
    assert feed_details === %{htmlUrl: "https://www.printmesomecolor.com", title: "Print Me Some Color"}
    Logger.debug "==========================================================================================="
  end

  test "test feedburner site" do
    Logger.debug "==========================================================================================="
    feed_url = FeedFinder.get_feed_xml_url("https://feeds.feedburner.com/TheEdublogger")
    feed_details = FeedFinder.get_feed_details(feed_url)
    assert feed_details === %{htmlUrl: "https://www.theedublogger.com", title: "The Edublogger"}
    Logger.debug "==========================================================================================="
  end

end
